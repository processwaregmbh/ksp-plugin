# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
 /***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

import os
from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'rollenauswahl.ui'))

class RollenauswahlDialog(QtGui.QDialog, FORM_CLASS):

    def __init__(self, parent=None):
        """Constructor."""
        super(RollenauswahlDialog, self).__init__(parent)
        self.setupUi(self)