# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
 /***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_neuesObjektDlg(object):
    def setupUi(self, neuesObjektDlg, elemente):
        neuesObjektDlg.setObjectName(_fromUtf8("neuesObjektDlg"))
        neuesObjektDlg.resize(460, 153)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(neuesObjektDlg.sizePolicy().hasHeightForWidth())
        neuesObjektDlg.setSizePolicy(sizePolicy)
        self.gridLayout_2 = QtGui.QGridLayout(neuesObjektDlg)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnAbbrechen = QtGui.QPushButton(neuesObjektDlg)
        self.btnAbbrechen.setObjectName(_fromUtf8("btnAbbrechen"))
        self.horizontalLayout.addWidget(self.btnAbbrechen)
        self.btnOK = QtGui.QPushButton(neuesObjektDlg)
        self.btnOK.setObjectName(_fromUtf8("btnOK"))
        self.horizontalLayout.addWidget(self.btnOK)
        self.gridLayout_2.addLayout(self.horizontalLayout, 4, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.typDerGeometrieComboBox = QtGui.QComboBox(neuesObjektDlg)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.typDerGeometrieComboBox.sizePolicy().hasHeightForWidth())
        self.typDerGeometrieComboBox.setSizePolicy(sizePolicy)
        self.typDerGeometrieComboBox.setMaximumSize(QtCore.QSize(550, 26))
        self.typDerGeometrieComboBox.setObjectName(_fromUtf8("typDerGeometrieComboBox"))
        
        self.gridLayout.addWidget(self.typDerGeometrieComboBox, 3, 1, 1, 1)
        self.kennungLineEdit = QtGui.QLineEdit(neuesObjektDlg)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.kennungLineEdit.sizePolicy().hasHeightForWidth())
        self.kennungLineEdit.setSizePolicy(sizePolicy)
        self.kennungLineEdit.setMaximumSize(QtCore.QSize(550, 22))
        self.kennungLineEdit.setObjectName(_fromUtf8("kennungLineEdit"))
        self.gridLayout.addWidget(self.kennungLineEdit, 1, 1, 1, 1)
        self.kennungLabel = QtGui.QLabel(neuesObjektDlg)
        self.kennungLabel.setObjectName(_fromUtf8("kennungLabel"))
        self.gridLayout.addWidget(self.kennungLabel, 1, 0, 1, 1)
        self.typDerGeometrieLabel = QtGui.QLabel(neuesObjektDlg)
        self.typDerGeometrieLabel.setObjectName(_fromUtf8("typDerGeometrieLabel"))
        self.gridLayout.addWidget(self.typDerGeometrieLabel, 3, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem1, 3, 0, 1, 1)

        self.retranslateUi(neuesObjektDlg, elemente)
        QtCore.QMetaObject.connectSlotsByName(neuesObjektDlg)
        neuesObjektDlg.setTabOrder(self.btnAbbrechen, self.btnOK)

    def retranslateUi(self, neuesObjektDlg, elemente):
        neuesObjektDlg.setWindowTitle(_translate("neuesObjektDlg", "neues Objekt", None))
        self.btnAbbrechen.setText(_translate("neuesObjektDlg", "Abbrechen", None))
        self.btnOK.setText(_translate("neuesObjektDlg", "OK", None))
        self.kennungLabel.setText(_translate("neuesObjektDlg", "Kennung", None))
        self.typDerGeometrieLabel.setText(_translate("neuesObjektDlg", "Typ der Geometrie", None))
        if elemente is not None:
            for e in elemente:
                self.typDerGeometrieComboBox.addItem(_fromUtf8(e.get('name')), e)
        

