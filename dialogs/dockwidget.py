# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
/***************************************************************************
 DiviPluginDockWidget
                                 A QGIS plugin
 Integracja QGIS z platformą DIVI firmy GIS Support sp. z o. o.
                             -------------------
        begin                : 2016-02-09
        git sha              : $Format:%H$
        copyright            : (C) 2016 by GIS Support sp. z o. o.
        email                : info@gis-support.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
from functools import partial

from PyQt4 import QtGui, QtCore, uic
from PyQt4.QtCore import pyqtSignal, QSettings, Qt, QRegExp, QUrl
from PyQt4.QtNetwork import QNetworkRequest, QNetworkAccessManager
from qgis.core import QgsMessageLog, QgsMapLayerRegistry, QgsVectorLayer, QGis, QgsApplication, QgsFeature, QgsGeometry, QgsCoordinateReferenceSystem, QgsSymbolV2, QgsRectangle
from qgis.gui import QgsMessageBar, QgsMessageBarItem, QgsFilterLineEdit
from ..utils.connector import DiviConnector
from ..utils.model import *
from ..utils.widgets import ProgressMessageBar
from PyQt4.QtWebKit import *
from sachdaten.browser import Ui_Browser
from sachdaten.browser_dialog import BrowserDlg
from objektanlegen.neuesobjekt import Ui_neuesObjektDlg
from sachdatenbearbeiten.weiter import Ui_weiterDlg
from einstellungen.einstellungen import Ui_Einstellungen
from rollenauswahl.rollenauswahl import RollenauswahlDialog
from PyQt4.Qt import QDialog

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'dockwidget.ui'))


class DiviPluginDockWidget(QtGui.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, plugin, parent=None):
        """Constructor."""
        super(DiviPluginDockWidget, self).__init__(parent)
        QWebView.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.plugin = plugin
        self.iface = plugin.iface
        self.token = QSettings().value('komon/token', None)
        self.user = QSettings().value('komon/email', None)
        self.urlserver = QSettings().value('komon/urlserver', '')
        self.setupUi(self)
        self.initGui()
        self.isneu = True
        
        conn = self.getConnector(auto_login=False)
        if self.urlserver is not None:
            self.konfiguration = conn.getKonfiguration();
        
        proxyModel = DiviProxyModel()
        proxyModel.setSourceModel( DiviModel() )
        proxyModel.setDynamicSortFilter( True )
        self.tvData.setModel( proxyModel )
        self.tvData.setSortingEnabled(True)
        self.setLoginStatus(bool(self.token))
        
        self.gispadid = 0
        self.okl = ''
        
        self.diviConnection(checked=False, auto_login=False)
        #Signals
        self.btnConnect.clicked.connect(self.diviConnection)
        self.btnObjektNeu.clicked.connect(self.neuesObjektDlgAnzeigen)
        self.btnObjektSpeichern.clicked.connect(self.saveGeom)
        self.btnObjektLoeschen.clicked.connect(self.deleteObject)
        self.eSearch.textChanged.connect(self.searchData)
        self.tvData.doubleClicked.connect(self.addLayer)
        QgsMapLayerRegistry.instance().layersWillBeRemoved.connect( self.layersRemoved )
    
    def initGui(self):
        self.eSearch = QgsFilterLineEdit(self.dockWidgetContents)
        self.eSearch.setObjectName(u"eSearch")
        self.eSearch.setPlaceholderText(self.tr("Suche..."))
        self.editLayout.addWidget(self.eSearch)
    
    def getConnector(self, auto_login=False):
        connector = DiviConnector(iface=self.iface, auto_login=auto_login)
        #connector.diviLogged.connect(self.setUserData)
        self.token = QSettings().value('komon/token', None)
        self.userkey = QSettings().value('komon/userkey', None)
        self.email = QSettings().value('komon/email', None)
        return connector
    
    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()
    
    def diviConnection(self, checked = False, auto_login=False):
        QgsMessageLog.logMessage(str(u"Verbindung zum KSP-Server:"), 'KSP')
        model = self.tvData.model().sourceModel()
        items = {}
        if checked:
            #Connect
            model.showLoading()
            connector = self.getConnector(auto_login)
            #connector = self.getConnector(False)
            
            if self.konfiguration is None:
                self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Die Konfiguration des Plugins konnte nicht ermittelt werden. Vielleicht ist die Serveranwendung nicht erreichbar. Beenden Sie QGis und versuchen Sie später noch ein mal.'),
                    self.iface.messageBar().CRITICAL,
                    duration = 0
                )
            token = QSettings().value('komon/token')
            if token is None:
                connector.diviLogin()
            
            token = QSettings().value('komon/token')
            if token is not None:
                self.setLoginStatus(True)    
                 
                items = connector.diviFeatchData(self.konfiguration)
                if items is not None and self.konfiguration['objektklassen'] is not None:
                    model.clearData()
                    model.addData(self.konfiguration['objektklassen'], "OKL")
                    for o in self.konfiguration['objektklassen']:
                        model.addData(items[o['name']], o['name'])
                    
                    self.getLoadedDiviLayers()
                    return
                else:
                    model.removeAll()
            else:
                self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Der Nutzer konnte nicht angemeldet werden oder Sie haben keine Berechtigung für den Datenzugriff über dieses Plugin. Überprüfen Sie bitte Ihre Anmeldedaten und versuchen Sie es erneut.'),
                    self.iface.messageBar().CRITICAL,
                    duration = 0
                )
            
                
        QSettings().remove('komon/token')
        QSettings().remove('komon/userkey')
        self.setLoginStatus(False)
    
    def setLoginStatus(self, logged):
        if logged:
            self.lblStatus.setText(self.tr(u'Angemeldet als: %s') % str(QSettings().value('komon/email', None)))
            self.btnConnect.setText(self.tr('Abmelden'))
            self.btnConnect.setChecked(True)
            self.token = QSettings().value('komon/token', None)
            self.btnObjektLoeschen.setEnabled(False);
            self.btnObjektSpeichern.setEnabled(False);
            self.btnObjektNeu.setEnabled(True);
        else:
            self.tvData.model().sourceModel().removeAll()
            self.lblStatus.setText(self.tr('Nicht angemeldet                         '))
            self.btnConnect.setText(self.tr('Anmelden'))
            self.btnConnect.setChecked(False)
            self.token = None
            self.userkey = None
            self.btnObjektLoeschen.setEnabled(False);
            self.btnObjektSpeichern.setEnabled(False);
            self.btnObjektNeu.setEnabled(False);
            QSettings().remove('komon/token')
            QSettings().remove('komon/userkey')
            self.token = None
            self.userkey = None
            self.email = None
        #QgsMessageLog.logMessage(str(self.token), 'KSP')
    
    def setUserData(self, user, token, userkey):
        self.user = user
        self.token = token
        self.userkey = userkey
        if token:
            self.setLoginStatus(True)
    
    def getLoadedDiviLayers(self, layers=None):
        if layers is None:
            layers = [ layer for layer in QgsMapLayerRegistry.instance().mapLayers().itervalues() if layer.customProperty('DiviId') is not None ]
        model = self.tvData.model().sourceModel()
        for layer in layers:
            divi_id = layer.customProperty('DiviId')
            item_type = 'vector'
            layerIndex = model.findItem(divi_id, item_type, True)
            if layerIndex is not None:
                layerItem = layerIndex.data(role=Qt.UserRole)
                if layer not in layerItem.items:
                    self.plugin.registerLayer(layer, divi_id, [], {}, False, layerItem.fields)
                    layerItem.items.append(layer)
                    model.dataChanged.emit(layerIndex, layerIndex)
    
    #SLOTS
    
    def addLayer(self, index):
        item = index.data(role=Qt.UserRole)
        addedData = []
        #TODO: Add loading rasters
        if isinstance(item, ModelItem):
            self.iface.messageBar().pushMessage("KSP",self.tr(u"Die Geometrie des Objektes '%s' wird heruntergeladen ...")%item.name,level=QgsMessageBar.INFO, duration=1)
            self.addGeom(index)
        else:
            return
        
        index.model().dataChanged.emit(index.parent().parent(), index)
        return addedData
    
    def layersRemoved(self, layers):
        removed_ids = set([])
        model = self.tvData.model().sourceModel()
        try:
            for lid in layers:
                layer = QgsMapLayerRegistry.instance().mapLayer(lid)
                divi_id = layer.customProperty('DiviId')
                if divi_id is None:
                    continue
                QgsMessageLog.logMessage(self.tr('Removing layer %s')%layer.name(), 'KSP')
                item_type = 'table' if layer.geometryType()==QGis.NoGeometry else 'vector'
                layerIndex = model.findItem(divi_id, item_type, True)
                if layerIndex is None:
                    continue
                layerItem = layerIndex.data(role=Qt.UserRole)
                if layer in layerItem.items:
                    layerItem.items.remove(layer)
                    self.plugin.unregisterLayer(layer)
                    model.dataChanged.emit(layerIndex, layerIndex)
        except:
            pass
    
    def searchData(self, text):
        self.tvData.model().setFilterRegExp(QRegExp(text, Qt.CaseInsensitive, QRegExp.FixedString))
        if text:
            self.tvData.expandAll()
        else:
            self.tvData.collapseAll()
            
    def addGeom(self, index):
        item = index.data(role=Qt.UserRole)
        layers = QgsMapLayerRegistry.instance().mapLayers().values()
        istda = False
        layer = None
        canvas = self.iface.mapCanvas()
        offset = 50
        
        self.gispadid = str(item.id)
        self.okl = item.okl;
        connector = self.getConnector()
        content = connector.loadGeom(self.gispadid, self.okl.lower())
        
        typ = -1;
        typname = '';
        geom_ = '';
        if content is not None and len(content) > 0:
            try:
                geom = content[0]['geom']
            except:
                self.iface.messageBar().popWidget()
                self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Die Geometrie konnte nicht geladen werden.'),
                    self.iface.messageBar().CRITICAL,
                    duration = 3
                )
                return
        else:
            self.iface.messageBar().popWidget()
            self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Die Geometrie konnte nicht geladen werden.'),
                    self.iface.messageBar().CRITICAL,
                    duration = 3
                )
            return
        if geom.startswith('MULTIPOLYGON') or geom.startswith('POLYGON'):
            typ = 2;
            typname = 'Polygon'
        elif geom.startswith('MULTILINE') or geom.startswith('LINE'):
            typ = 1;
            typname = 'Linestring'
        elif geom.startswith('MULTIPOINT') or geom.startswith('POINT'):
            typ = 0;
            typname = 'Point'
        
        istda, layer = self.deleteEditLayer(typ)
            
        if not istda:
            layer =  QgsVectorLayer(typname + '?crs=epsg:25832', 'Service-Portal-Layer' , "memory")
            if layer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(layer)
                symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())
                symbol.setColor(QtGui.QColor.fromRgb(220,20,60))
                layer.rendererV2().setSymbol(symbol)
                self.iface.legendInterface().refreshLayerSymbology(layer)
            else:
                self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Der KSP-Layer konnte nicht hinzugefügt werden.'),
                    self.iface.messageBar().CRITICAL,
                    duration = 3
                )
                return
        
        if layer is not None:
            it = layer.getFeatures() 
            ids = [i.id() for i in it]
            pr = layer.dataProvider()
            pr.deleteFeatures(ids)
            
            for geom in content:
                feature = QgsFeature()
                feature.setGeometry( QgsGeometry.fromWkt(geom['geom']) )
                feature.setAttributes([self.gispadid])
                pr.addFeatures([feature])
                layer.updateExtents()
                zoomRectangle = QgsRectangle(layer.extent().xMinimum()-offset, layer.extent().yMinimum()-offset,layer.extent().xMaximum()+offset,layer.extent().yMaximum()+offset)
                canvas.setExtent(zoomRectangle)
                canvas.refresh()
                
            self.btnObjektLoeschen.setEnabled(True);
            self.btnObjektSpeichern.setEnabled(True);
            self.isneu = False
                
    def deleteObject(self):
        if self.gispadid > 0:
            
            connector = self.getConnector()
            json = connector.deleteObject(self.gispadid, self.okl.lower())
            self.deleteEditLayer()
            
            if json and json['message'] == "ok":
                model = self.tvData.model().sourceModel()
                index = model.findItem(u'' + self.gispadid, self.okl, True)
                model.removeRows(index.row(),1,index.parent())
                self.btnObjektLoeschen.setEnabled(False);
                self.btnObjektSpeichern.setEnabled(False);
                
                self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Das Objekt wurde gelöscht.'),
                    self.iface.messageBar().SUCCESS,
                    duration = 3
                )
            else:
                self.iface.messageBar().pushMessage('KSP',
                    self.tr(u'Beim Löschen des Objektes ist ein Fehler aufgetreten: ' + json['message']),
                    self.iface.messageBar().CRITICAL,
                    duration = 3
                )
            
    def saveGeom(self, index):
        try:
            self.iface.messageBar().pushItem(QgsMessageBarItem('KSP', u'Die Geometrie wird gespeichert...'))
            layer = None
            test = 0
            
            layerList = QgsMapLayerRegistry.instance().mapLayersByName("Service-Portal-Layer")
            if layerList: 
                layer = layerList[0]
            
            layer.commitChanges()
            
            geom = QgsGeometry.fromWkt('GEOMETRYCOLLECTION()')
            anzahl = 0;
            
            for feature in layer.getFeatures():
                geom = geom.combine(feature.geometry());
            
            if geom is not None:       
                wkt = geom.exportToWkt()
                geoj = geom.exportToGeoJSON()
                    
                connector = self.getConnector()
                
                trenner = "/"
                url = QSettings().value('komon/urlserver', '')
                if url is None or url.strip() == '':
                    self.iface.messageBar().pushMessage('Fehler',
                        self.tr('Der URL des Servers ist leer.'),
                        self.iface.messageBar().CRITICAL,
                        duration = 3
                    )
                else:
                    if url.strip().endswith('/'):
                        trenner = ''
                    url = QSettings().value('komon/urlserver', '') + trenner + "wc?action=qgisobjekt&id=%s&token=%s&okl=%s" % (self.gispadid, self.userkey, self.okl)
                    myapp = Ui_Browser()
                    d = BrowserDlg()
                    myapp.setupUi(d, self)
                    request = QNetworkRequest()
                    request.setUrl(QUrl(url))
                    request.setRawHeader("Authorization", self.token);
                    
                    #&id=%s&token=%s&okl=%s" % (self.gispadid, self.userkey, self.okl)
                    
                    myapp.webView.load(request, QNetworkAccessManager.PostOperation, "wkt=" + wkt + "&kennung=" + self.kennung)
                    
                    d.show()
                    d.exec_()
                
            else:
                self.iface.messageBar().pushMessage('KSP',
                        self.tr(u'Beim Auslesen der Geometrie ist ein Fehler aufgetreten.'),
                        self.iface.messageBar().CRITICAL,
                        duration = 3
                    )
        except:
            self.iface.messageBar().pushMessage('KSP',
                self.tr(u'Beim Speichen der Geometrie ist ein Fehler aufgetreten.'),
                self.iface.messageBar().CRITICAL,
                duration = 3
            )
        finally:
            self.iface.messageBar().popWidget()
    
    def unregisterLayers(self, item):
        if isinstance(item, AccountItem):
            for project in item.childItems:
                self.unregisterLayers(project)
        else:
            for child in item.childItems:
                for layer in child.items:
                    self.plugin.unregisterLayer(layer)
                    
    def deleteEditLayer(self, typ = "kein"):
        layers = QgsMapLayerRegistry.instance().mapLayers().values()
        istda = False
        layer = None
        
        for layer in layers:
            istda = False
            if layer.name() == "Service-Portal-Layer":
                geomtype = layer.geometryType()
                if(geomtype != typ):
                    QgsMapLayerRegistry.instance().removeMapLayer(layer.id())
                else:
                    istda = True    
                break;
            
        return istda, layer
    
    def createEditLayer(self, typname):
        layer =  QgsVectorLayer(typname + '?crs=epsg:25832', 'Service-Portal-Layer' , "memory")
        if layer.isValid():
            QgsMapLayerRegistry.instance().addMapLayer(layer)
            symbols = layer.rendererV2().symbols()
            symbol = symbols[0]
            symbol.setColor(QtGui.QColor.fromRgb(220,20,60))
            self.iface.legendInterface().refreshLayerSymbology(layer)
            layer.startEditing()
        else:
            self.iface.messageBar().pushMessage("KSP",u'Der Service-Portal-Layer konnte nicht hinzugefügt werden.', QgsMessageBar.Error, 3)
            return
            
    def neuesObjektDlgAnzeigen(self):
        test = False
        
        if self.gispadid > 0:
            reply = QtGui.QMessageBox.question(self.iface.mainWindow(), 'Neues Objekt erzeugen?', u'Wenn Sie fortfahren werden alle Änderungen verworfen. Möchten Sie fortfahren?', QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                test = True
        else:
            test = True
            
        if test:
            self.neuesobjekt = Ui_neuesObjektDlg()
            d = QDialog()
            self.neuesobjekt.setupUi(d, self.konfiguration.get('geotypen'))
            self.neuesobjekt.dialog = d;
            QtCore.QObject.connect(self.neuesobjekt.btnOK, QtCore.SIGNAL("clicked()"), self.neuesobjektOnOK)
            QtCore.QObject.connect(self.neuesobjekt.btnAbbrechen, QtCore.SIGNAL("clicked()"), self.neuesobjektOnAbbrechen)
            self.neuesobjekt.dialog.exec_()
            self.btnObjektSpeichern.setEnabled(True);
            self.isneu = True
        
    def neuesobjektOnOK(self):
        okl = ""
        typ = ""
        
        kennung = self.neuesobjekt.kennungLineEdit.text()
        val = str(self.neuesobjekt.typDerGeometrieComboBox.currentText())
        itemData = self.neuesobjekt.typDerGeometrieComboBox.itemData(self.neuesobjekt.typDerGeometrieComboBox.currentIndex())
        
        okl = itemData.get('okl')
        typ = itemData.get('typ')
        kuerzel = itemData.get('kuerzel')
            
        if kennung is None or len(kennung.strip()) == 0:
            kennung = "";
        else:
            if not kennung.startswith(okl.upper() + "-"):
                kennung = kuerzel.upper() + "-" + kennung
            connector = self.getConnector()
            test = connector.checkKennung(kennung, okl)
            if test == False:
                reply = QtGui.QMessageBox.question(self.iface.mainWindow(), 'Kennung bereits vergeben', u'Diese Kennung wurde bereits vergeben. Geben Sie bitte eine neue eindeutuge Kennung ein oder lassen Sie das Feld leer, um eine Kennung automatisch generieren zu lassen.', QtGui.QMessageBox.Yes)
                if reply == QtGui.QMessageBox.Yes:
                    return
                
        
        self.deleteEditLayer()
        self.createEditLayer(typ)
        self.gispadid = 0
        self.okl = okl
        self.kennung = kennung
        #connector = self.getConnector()
        #content = connector.createNewObject(self.okl.lower(), kennung)
        self.neuesobjekt.dialog.close()
             
        
    def neuesobjektOnAbbrechen(self):
        self.neuesobjekt.dialog.close()
        
    def weiterOnJa(self, close_window = True):
        trenner = "/"
        url = QSettings().value('komon/urlserver', '')
        if url is None or url.strip() == '':
            self.iface.messageBar().pushMessage('Fehler',
                self.tr('Der URL des Servers ist leer.'),
                self.iface.messageBar().CRITICAL,
                duration = 3
            )
        else:
            if url.strip().endswith('/'):
                trenner = ''
            url = QSettings().value('komon/urlserver', '') + trenner + "wc?action=qgisobjekt&id=%s&token=%s&okl=%s" % (self.gispadid, self.userkey, self.okl)
            myapp = Ui_Browser()
            d = BrowserDlg()
            myapp.setupUi(d, self)
            request = QNetworkRequest()
            request.setUrl(QUrl(url))
            request.setRawHeader("Authorization", self.token);
            myapp.webView.load(request)
            if close_window:
                self.weiter.dialog.close()
            
            d.show()
            d.exec_()
            
    def createNewObject(self):
        trenner = "/"
        url = QSettings().value('komon/urlserver', '')
        if url is None or url.strip() == '':
            self.iface.messageBar().pushMessage('Fehler',
                self.tr('Der URL des Servers ist leer.'),
                self.iface.messageBar().CRITICAL,
                duration = 3
            )
        else:
            if url.strip().endswith('/'):
                trenner = ''
            url = QSettings().value('komon/urlserver', '') + trenner + "wc?action=qgisobjekt&id=%s&token=%s&okl=%s" % (self.gispadid, self.userkey, self.okl)
            myapp = Ui_Browser()
            d = BrowserDlg()
            myapp.setupUi(d, self)
            request = QNetworkRequest()
            request.setUrl(QUrl(url))
            request.setRawHeader("Authorization", self.token);
            
            #&id=%s&token=%s&okl=%s" % (self.gispadid, self.userkey, self.okl)
            
            myapp.webView.load(request, QNetworkAccessManager.PostOperation, "wkt")
            
            d.show()
            d.exec_()
        
    def weiterOnNein(self):
        self.weiter.dialog.close()
        
