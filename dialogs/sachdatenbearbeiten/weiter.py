# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
 /***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_weiterDlg(object):
    def setupUi(self, weiterDlg):
        weiterDlg.setObjectName(_fromUtf8("weiterDlg"))
        weiterDlg.resize(460, 121)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(weiterDlg.sizePolicy().hasHeightForWidth())
        weiterDlg.setSizePolicy(sizePolicy)
        self.verticalLayout_2 = QtGui.QVBoxLayout(weiterDlg)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(weiterDlg)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnNein = QtGui.QPushButton(weiterDlg)
        self.btnNein.setObjectName(_fromUtf8("btnNein"))
        self.horizontalLayout.addWidget(self.btnNein)
        self.btnJa = QtGui.QPushButton(weiterDlg)
        self.btnJa.setObjectName(_fromUtf8("btnJa"))
        self.horizontalLayout.addWidget(self.btnJa)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(weiterDlg)
        QtCore.QMetaObject.connectSlotsByName(weiterDlg)
        weiterDlg.setTabOrder(self.btnNein, self.btnJa)

    def retranslateUi(self, weiterDlg):
        weiterDlg.setWindowTitle(_translate("weiterDlg", "Sachdaten bearbeiten?", None))
        self.label.setText(_translate("weiterDlg", "Die Geometrie wurde gespeichert. Möchten Sie die Sachdaten bearbeiten?", None))
        self.btnNein.setText(_translate("weiterDlg", "Nein", None))
        self.btnJa.setText(_translate("weiterDlg", "Ja", None))

