# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
/***************************************************************************
 DiviPluginDockWidget
                                 A QGIS plugin
 Integracja QGIS z platformą DIVI firmy GIS Support sp. z o. o.
                             -------------------
        begin                : 2016-02-09
        git sha              : $Format:%H$
        copyright            : (C) 2016 by GIS Support sp. z o. o.
        email                : info@gis-support.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Einstellungen(object):
    def setupUi(self, Einstellungen):
        Einstellungen.setObjectName(_fromUtf8("Einstellungen"))
        Einstellungen.resize(400, 83)
        self.verticalLayout = QtGui.QVBoxLayout(Einstellungen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.lblURL = QtGui.QLabel(Einstellungen)
        self.lblURL.setObjectName(_fromUtf8("lblURL"))
        self.horizontalLayout.addWidget(self.lblURL)
        self.txtURL = QtGui.QLineEdit(Einstellungen)
        self.txtURL.setObjectName(_fromUtf8("txtURL"))
        self.horizontalLayout.addWidget(self.txtURL)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem = QtGui.QSpacerItem(20, 1, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem1 = QtGui.QSpacerItem(62, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.btnJa = QtGui.QPushButton(Einstellungen)
        self.btnJa.setObjectName(_fromUtf8("btnJa"))
        self.horizontalLayout_2.addWidget(self.btnJa)
        self.btnNein = QtGui.QPushButton(Einstellungen)
        self.btnNein.setObjectName(_fromUtf8("btnNein"))
        self.horizontalLayout_2.addWidget(self.btnNein)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(Einstellungen)
        QtCore.QMetaObject.connectSlotsByName(Einstellungen)

    def retranslateUi(self, Einstellungen):
        Einstellungen.setWindowTitle(_translate("Einstellungen", "Einstellungen", None))
        self.lblURL.setText(_translate("Einstellungen", "URL des Servers", None))
        self.btnJa.setText(_translate("Einstellungen", "OK", None))
        self.btnNein.setText(_translate("Einstellungen", "Abbrechen", None))

