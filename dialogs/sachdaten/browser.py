# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
 /***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Browser(object):
    def setupUi(self, Browser, plugin):
        Browser.setObjectName(_fromUtf8("Browser"))
        Browser.resize(1248, 800)
        self.browser = Browser
        self.plugin = plugin
        self.gridLayout = QtGui.QGridLayout(self.browser)
        self.gridLayout.setContentsMargins(0, 0, 0, 5)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.webView = QtWebKit.QWebView(Browser)
        
        self.webView.settings().setAttribute(QWebSettings.DeveloperExtrasEnabled, True)
        
        self.webView.setUrl(QtCore.QUrl(_fromUtf8("about:blank")))
        self.webView.setObjectName(_fromUtf8("webView"))
        self.gridLayout_2.addWidget(self.webView, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(Browser)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 1)

        self.retranslateUi(self.browser)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), self.sendData)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Browser.reject)
        
        QtCore.QMetaObject.connectSlotsByName(self.browser)

    def sendData(self):
        page = self.webView.page()
        QtCore.QObject.connect(self.webView.page(), QtCore.SIGNAL(_fromUtf8("loadFinished(bool)")), self.loadFinished)
        el = page.mainFrame().findFirstElement("input#form\\:speichern");
        el.evaluateJavaScript("this.click()");
        
    def loadFinished(self):
        page = self.webView.page()
        el = page.mainFrame().findFirstElement("li.error");
        qgisid_el = page.mainFrame().findFirstElement("input#form\\:id");
        page.mainFrame().toHtml()
        qgisid = 0
        if not qgisid_el.isNull():
            qgisid = int(qgisid_el.attribute('value'))
        
        if el.isNull() and qgisid > 0:
            self.browser.close()
            model = self.plugin.tvData.model().sourceModel()
            connector = self.plugin.getConnector()
            content = connector.loadGeom(str(qgisid), self.plugin.okl.lower())
            
            if content is not None and len(content) > 0:
                tmp = content[0]
            
            obj = {}
            obj['name'] = tmp.get('name')
            obj['id'] = tmp.get('id')
            obj['okl'] = tmp.get('okl')
            obj['description'] = tmp.get('description')
            obj['id_accounts'] = tmp.get('id_accounts')
            
            list = [obj]
            
            okl = model.findItem(self.plugin.okl.upper(), 'okl', True)
            if okl and self.plugin.isneu:
                model.addData(list, self.plugin.okl)
                
            self.plugin.isneu = False
        
    
    def retranslateUi(self, Browser):
        Browser.setWindowTitle(_translate("Browser", "Sachdaten pflegen", None))

from PyQt4 import QtWebKit
from PyQt4.QtWebKit import QWebSettings
