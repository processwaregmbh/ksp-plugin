# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
 /***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

from PyQt4 import QtCore, QtGui
from PyQt4 import QtWebKit

class BrowserDlg(QtGui.QDialog):
    
    def __init__(self):
        QtGui.QDialog.__init__(self)
    
    def accept1111(self, webView):
        
        page = webView.page()
        frame = page.currentFrame()
        
        msgBox = QtGui.QMessageBox()
        msgBox.setText(frame.frameName())
        msgBox.exec_()
    
    def createMsgDialog(self, text):
        msgBox = QtGui.QMessageBox()
        msgBox.setText(text)
        msgBox.exec_()