# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
/***************************************************************************
 model
                                 A QGIS plugin
 Integracja QGIS z platformą DIVI firmy GIS Support sp. z o. o.
                             -------------------
        begin                : 2016-02-09
        git sha              : $Format:%H$
        copyright            : (C) 2016 by GIS Support sp. z o. o.
        email                : info@gis-support.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.QtCore import QObject, QAbstractItemModel, Qt, QModelIndex, SIGNAL
from PyQt4.QtGui import QIcon, QFont, QSortFilterProxyModel
from qgis.core import QgsMessageLog
import locale
import os.path

class TreeItem(QObject):
    '''
    a python object used to return row/column data, and keep note of
    it's parents and/or children
    '''
    def __init__(self, item, parentItem):
        super(TreeItem, self).__init__(parentItem)
        self.itemData = item
        self.childItems = []
        self.plugin_dir = os.path.dirname(__file__)
        
        if parentItem:
            parentItem.appendChild(self)

    def appendChild(self, item):
        self.childItems.append(item)
        self.connect(item, SIGNAL("itemRemoved"), self.childRemoved)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return 1
    
    def childRemoved(self):
        self.itemRemoved()

    def itemChanged(self):
        self.emit( SIGNAL("itemChanged"), self )
    
    def itemRemoved(self):
        self.emit(SIGNAL("itemRemoved"), self)
    
    def data(self, column):
        return "" if column == 0 else None
    
    def row(self):
        if self.parent():
            return self.parent().childItems.index(self)
        return 0
    
    def removeChild(self, row):
        if row >= 0 and row < len(self.childItems):
            self.childItems[row].itemData.deleteLater()
            self.disconnect(self.childItems[row], SIGNAL("itemRemoved"), self.childRemoved)
            del self.childItems[row]
    
    def removeChilds(self):
        for i in reversed(range(self.childCount())):
            self.removeChild(i)
    
    def removeChilds(self, row, count):
        for i in range(row, count):
            self.removeChild(i)

class LoadingItem(TreeItem):
    
    def __init__(self, parent=None):
        super(LoadingItem, self).__init__(self, parent)
        self.name = self.tr(u'Daten werden geladen...')
        
        self.icon = QIcon(os.path.join(self.plugin_dir,'../images/downloading.png'))
    
class OKLItem(TreeItem):
    
    def __init__(self, data, parent=None):
        super(OKLItem, self).__init__(self, parent)
        self.name = data.get('name')
        self.id = data.get('name')
        self.abstract = data.get('name')
    
    def identifier(self):
        return 'okl@%s' % self.id
    
    def loadedChilds(self):
        """ True if any layer/table from this account is loaded """
        return any( child.loadedChilds() for child in self.childItems )

class ModelItem(TreeItem):
    
    def __init__(self, data, parent=None):
        super(ModelItem, self).__init__(self, parent)
        self.name = data.get('name')
        self.id = data.get('id')
        self.okl = data.get('okl')
        self.id_accounts = data.get('id_accounts')
        self.abstract = data.get('description')
        self.icon = QIcon(os.path.join(self.plugin_dir,'../images/vector.png'))
        
    
    def identifier(self):
        return '%s@%s' % (self.okl, self.id)
    
    def loadedChilds(self):
        """ True if any layer/table from this project is loaded """
        return any( bool(child.items) for child in self.childItems )

class DiviModel(QAbstractItemModel):
    
    def __init__(self, parent=None):
        super(DiviModel, self).__init__(parent)
        
        self.rootItem = TreeItem(None, None)
    
    def columnCount(self, parent):
        return 1
    
    def rowCount(self, parent):
        parentItem = parent.data(Qt.UserRole) if parent.isValid() else self.rootItem
        return parentItem.childCount()
    
    def data(self, index, role):
        if not index.isValid():
            return None
        
        item = index.internalPointer()
        if role == Qt.DisplayRole:
            return item.name
        elif role == Qt.DecorationRole and hasattr(item, 'icon'):
            return item.icon
        elif role == Qt.FontRole:
            pass
        elif role == Qt.ToolTipRole:
            #return item.abstract
            return ""
        elif role == Qt.UserRole:
            #Return item itself
            return item
        elif role == Qt.UserRole+1:
            try:
                if item and item.identifier():
                    return item.identifier()
                else:
                    return ""
            except:
                return ""
        elif role == Qt.UserRole+2:
            return item.metaObject().className()
    
    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole and section == 0:
            return self.tr('Data')
        return None
    
    def showLoading(self):
        self.removeAll()
        self.beginInsertRows(QModelIndex(), 0, 0)
        item = LoadingItem( self.rootItem )
        self.endInsertRows()
        
    def addData(self, items, itemType):
        
        if itemType == "OKL":    
            self.addOKLs(items);
        else:
            self.addModelItems(items, itemType);
        
        
           
    def clearData(self):
        self.removeAll()
    
    def addOKLs(self, items):
        
        if items:
            self.beginInsertRows(QModelIndex(), 0, len(items) - 1)
            for okl in items:
                item = OKLItem(okl, self.rootItem)
            self.endInsertRows()
            
    def addModelItems(self, items, itemType):
        
        if items:
            parent = self.findItem(itemType, 'okl', True)
            okl = self.findItem(itemType, 'okl')
            self.beginInsertRows(parent, 0, len(items) - 1)
            for i in items:
                item = ModelItem(i, okl)
            self.endInsertRows()
    
    def removeAll(self):
        item = self.index(0,0).data(Qt.UserRole)
        self.removeRows(0, self.rootItem.childCount(), self.index(0,0).parent())
    
    def removeRows(self, row, count, parent):
        if not parent.isValid():
            item = self.rootItem
        else:
            item = parent.data(Qt.UserRole)
        self.beginRemoveRows(parent, row, count+row-1)
        item.removeChilds(row, count+row)
        self.endRemoveRows()
    
    def index(self, row, column, parent=QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()
        parentItem = parent.data(Qt.UserRole) if parent.isValid() else self.rootItem
        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        return QModelIndex()
    
    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = index.data(Qt.UserRole)
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def hasChildren(self, parent):
        parentItem = parent.data(Qt.UserRole) if parent.isValid() else self.rootItem
        return parentItem.childCount() > 0
    
    def findItem(self, oid, item_type='okl', as_model=False):
        if oid is None:
            return
        indexes = self.match(
            self.index(0,0),
            Qt.UserRole+1,
            '%s@%s' % (item_type, oid),
            -1,
            Qt.MatchRecursive
        )
        if indexes:
            if as_model:
                return indexes[0]
            else:
                return indexes[0].data(role=Qt.UserRole)

class DiviProxyModel(QSortFilterProxyModel):
    # Source: http://gaganpreet.in/blog/2013/07/04/qtreeview-and-custom-filter-models/
    ''' Class to override the following behaviour:
            If a parent item doesn't match the filter,
            none of its children will be shown.
 
        This Model matches items which are descendants
        or ascendants of matching items.
    '''
 
    def filterAcceptsRow(self, row_num, source_parent):
        ''' Overriding the parent function '''
 
        # Check if the current row matches
        if self.filter_accepts_row_itself(row_num, source_parent):
            return True
 
        # Traverse up all the way to root and check if any of them match
        if self.filter_accepts_any_parent(source_parent):
            return True
 
        # Finally, check if any of the children match
        return self.has_accepted_children(row_num, source_parent)
 
    def filter_accepts_row_itself(self, row_num, parent):
        return super(DiviProxyModel, self).filterAcceptsRow(row_num, parent)
 
    def filter_accepts_any_parent(self, parent):
        ''' Traverse to the root node and check if any of the
            ancestors match the filter
        '''
        while parent.isValid():
            if self.filter_accepts_row_itself(parent.row(), parent.parent()):
                return True
            parent = parent.parent()
        return False
 
    def has_accepted_children(self, row_num, parent):
        ''' Starting from the current node as root, traverse all
            the descendants and test if any of the children match
        '''
        model = self.sourceModel()
        source_index = model.index(row_num, 0, parent)
 
        children_count =  model.rowCount(source_index)
        for i in xrange(children_count):
            if self.filterAcceptsRow(i, source_index):
                return True
        return False
    
    def lessThan(self, left, right):
        lvalue = left.data()
        rvalue = right.data()
        if lvalue is None:
            return True
        if rvalue is None:
            return False
        return locale.strcoll(lvalue.lower(), rvalue.lower()) > 0

