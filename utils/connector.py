# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
/***************************************************************************
 diviConnector
                                 A QGIS plugin
 Integracja QGIS z platformą KomOn firmy GIS Support sp. z o. o.
                             -------------------
        begin                : 2016-02-09
        git sha              : $Format:%H$
        copyright            : (C) 2016 by GIS Support sp. z o. o.
        email                : info@gis-support.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.QtCore import QUrl, QObject, QEventLoop, pyqtSignal, QSettings, QBuffer, QPyNullVariant
from PyQt4.QtNetwork import QNetworkRequest, QNetworkAccessManager, \
    QHttpMultiPart, QHttpPart, QNetworkReply, QHttpMultiPart, QHttpPart, QNetworkProxy
from qgis.core import QgsMessageLog, QgsCredentials, QgsNetworkAccessManager
from qgis.gui import QgsMessageBar
from ..dialogs.rollenauswahl.rollenauswahl import RollenauswahlDialog
import json

class DiviConnector(QObject):
    diviLogged = pyqtSignal(str, str, str)
    downloadingProgress = pyqtSignal(float)
    uploadingProgress = pyqtSignal(float)
    
    KomOn_HOST_REST = '/service/qgis'
    
    def __init__(self, iface=None, auto_login=False):
        QObject.__init__(self)
        self.iface = iface
        self.auto_login = auto_login
        self.token = None
        self.token = QSettings().value('komon/token', None)
        self.userkey = QSettings().value('komon/userkey', None)
        self.email = QSettings().value('komon/email', None)
        #self.diviLogged.emit(self.email, self.token, self.userkey)
        #if auto_login and self.token is None:
        #    self.diviLogin()
    
    #Sending requests to KomOn
    
    def sendRequest(self, endpoint, params, method, data=None, headers={}):
        def send(params):
            url = self.formatUrl(endpoint, params)
            if url is None:
                return
            
            headers['User-Agent'] = 'KSP-Plugin'
            
            if self.token:
                if self.token.startswith('Bearer '):
                    headers['Authorization'] = self.token
                else:
                    headers['Authorization'] = 'Bearer ' + self.token
            
            request = QNetworkRequest(url)
            for key, value in headers.iteritems():
                request.setRawHeader(key, value)
            if method == 'delete':
                reply = manager.sendCustomRequest(request, 'DELETE', data)
            else:
                if not data:
                    reply = getattr(manager, method)(request)
                else:
                    reply = getattr(manager, method)(request, data)
            loop = QEventLoop()
            reply.uploadProgress.connect(self.uploadProgress)
            reply.downloadProgress.connect(self.downloadProgress)
            #reply.error.connect(self._error)
            reply.finished.connect(loop.exit)
            loop.exec_()
            return reply
        #manager=QgsNetworkAccessManager.instance()
        manager = QNetworkAccessManager()
        
        proxy = self.setProxy()
        if proxy:
            manager.setProxy(proxy)
            
        manager.sslErrors.connect(self._sslError)
        try:
            reply = send(params)
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            
        _headers = {}
        try:
            content = unicode(reply.readAll(),'utf8')
            rawheaders = reply.rawHeaderList()
            
            for hname in rawheaders:
                val = reply.rawHeader(hname)
                if val:
                    _headers[str(hname)] = str(val)
                    
        except:
            self.iface.messageBar().pushMessage(self.tr("Error"),
                self.trUtf8("Die Anfrage konne nicht gesendet werden."),
                level=QgsMessageBar.CRITICAL, duration=3)
            return
        
        status_code = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
        if reply.error() == QNetworkReply.ConnectionRefusedError:
            if self.iface is not None:
                self.iface.messageBar().pushMessage(self.tr("Error"),
                    self.trUtf8("Server rejected request"),
                    level=QgsMessageBar.CRITICAL, duration=3)
            return
        elif status_code == 403:
            if not self.auto_login:
                return
            #Invalid token, try to login and fetch data again
            result = self.diviLogin()
            if not result:
                return
            #Set new token
            params['token'] = result
            reply = send(params)
            content = reply.readAll()
        elif status_code == 404:
            if self.iface is not None:
                self.iface.messageBar().pushMessage(self.tr("Error"),
                    self.tr("Error 404: requested resource could not be found "),
                    level=QgsMessageBar.CRITICAL, duration=3)
            return
        elif status_code == 423:
            if self.iface is not None:
                self.iface.messageBar().pushMessage(self.tr("Error"),
                    self.tr("Error 423: requested resource is locked"),
                    level=QgsMessageBar.CRITICAL, duration=3)
            return
        return (content, _headers)
    
    def sendPostRequest(self, endpoint, data, params={}, headers={}):
        headers.update({"Content-Type":"application/json"})
        return self.sendRequest( endpoint, params,
                'post',
                json.dumps(data),
                headers=headers
            )
    
    def sendPutRequest(self, endpoint, data, params={}, headers={}):
        headers.update({"Content-Type":"application/json"})
        return self.sendRequest( endpoint, params,
                'put',
                json.dumps(data),
                headers=headers
            )
    
    def sendDeleteRequest(self, endpoint, data={}, params={}, headers={}):
        buff = QBuffer()
        buff.open(QBuffer.ReadWrite)
        buff.write(json.dumps(data).decode('utf-8'))
        buff.seek(0)
        headers.update({"Content-Type":"application/json"})
        content, response_headers = self.sendRequest( endpoint, params,
            'delete',
            buff,
            headers=headers
        )
        buff.close()
        return content, response_headers
    
    def sendGetRequest(self, endpoint, data={}):
        return self.sendRequest(endpoint, data, 'get')
    
    #Login
    
    def diviLogin(self):
        QgsMessageLog.logMessage('Fetching token', 'KSP')
        settings = QSettings()
        mail = settings.value('komon/email', None)
        if not mail:
            mail = None
        (success, email, password) = QgsCredentials.instance().get( 
            'Service-Portal-Login', mail, None )
        
        if not success:
            return
        
        content, response_headers = self.sendPostRequest('/nutzer/login', {
                'email': email,
                'password' : password,
                'remember': True
            })
        try:
            if response_headers is not None and response_headers['Authorization'] is not None:
                self.token = response_headers['Authorization']
            else:
                return
            
            data = json.loads(content)
            if 'message' in data:
                err = u'Die Anmeldung konnte nicht durchgefürt werden.'
                QgsMessageLog.logMessage(err, 'KSP')
                self.iface.messageBar().pushMessage('KSP',
                    err,
                    self.iface.messageBar().CRITICAL,
                    duration = 3
                )
                return
        except:
            return
        
        if len(data) == 0:
            return 
        elif len(data) == 1:
            loginnutzer = data[0]
            settings.setValue('komon/email', loginnutzer['user'])
            settings.setValue('komon/userkey', loginnutzer['userkey'])
            settings.setValue('komon/token', self.token)
            self.userkey = loginnutzer['userkey']
            #settings.setValue('komon/status', data['status'])
            #settings.setValue('komon/id', data['id'])
            #settings.setValue('komon/komkatasterstelle', data['komkatasterstelle'])
            self.diviLogged.emit(email, self.token, self.userkey)
            return self.userkey
            
        elif len(data) > 1:
            loginnutzer = data[0]
            self.rollenauswahl = RollenauswahlDialog()
            self.rollenauswahl.logindata = data
            self.rollenauswahl.rolleOK.clicked.connect(self.rolleUebernehmen)
            zahl = 0
            for a in data:
                self.rollenauswahl.rolleAuswahl.addItem(a['rolle'],"" + str(zahl))
                zahl = zahl + 1
            
            self.rollenauswahl.exec_()
            return
        
    def rolleUebernehmen(self):
        settings = QSettings()
        
        index = self.rollenauswahl.rolleAuswahl.currentIndex()
        loginnutzer = self.rollenauswahl.logindata[index]
        settings.setValue('komon/email', loginnutzer['user'])
        settings.setValue('komon/userkey', loginnutzer['userkey'])
        settings.setValue('komon/token', self.token)
        self.userkey = loginnutzer['userkey']
        #settings.setValue('komon/status', data['status'])
        #settings.setValue('komon/id', data['id'])
        #settings.setValue('komon/komkatasterstelle', data['komkatasterstelle'])
        self.rollenauswahl.close()
        self.diviLogged.emit(loginnutzer['user'], self.token, loginnutzer['userkey'])
    
    def setProxy(self):
        # procedure to set proxy if needed
        s = QSettings() #getting proxy from qgis options settings
        proxyEnabled = s.value("proxy/proxyEnabled", "")
        proxyType = s.value("proxy/proxyType", "" )
        proxyHost = s.value("proxy/proxyHost", "" )
        proxyPort = s.value("proxy/proxyPort", "" )
        proxyUser = s.value("proxy/proxyUser", "" )
        proxyPassword = s.value("proxy/proxyPassword", "" )
        if proxyEnabled: # test if there are proxy settings
            proxy = QNetworkProxy()
            if proxyType == "DefaultProxy":
                proxy.setType(QNetworkProxy.DefaultProxy)
            elif proxyType == "Socks5Proxy":
                proxy.setType(QNetworkProxy.Socks5Proxy)
            elif proxyType == "HttpProxy":
                proxy.setType(QNetworkProxy.HttpProxy)
            elif proxyType == "HttpCachingProxy":
                proxy.setType(QNetworkProxy.HttpCachingProxy)
            elif proxyType == "FtpCachingProxy":
                proxy.setType(QNetworkProxy.FtpCachingProxy)
            if proxyHost:
                proxy.setHostName(proxyHost)
            if proxyPort:
                proxy.setPort(int(proxyPort))
            if proxyUser:
                proxy.setUser(proxyUser)
            if proxyPassword:
                proxy.setPassword(proxyPassword)
           
            QNetworkProxy.setApplicationProxy(proxy)
            return proxy
    
    #Fetching data from server
    
    def diviFeatchConfiguration(self):
        QgsMessageLog.logMessage('Hole die Konfiguration', 'KSP')
        content, response_headers = self.sendGetRequest('/konfiguration')
        accounts = self.getJson(content)
        if not accounts:
            return None
        
        return config['data']
    
    def diviFeatchData(self, konfiguration):
        QgsMessageLog.logMessage('Hole die Daten', 'KSP')
        items = {}
        try:
            #content, response_headers = self.sendGetRequest('/nutzer')
            #accounts = self.getJson(content)
            #if not accounts:
            #    return
            
            #okls = self.getJson(self.sendGetRequest('/okl/namen', {'token':self.token}))
            for o in konfiguration['objektklassen']:
                content, response_headers = self.sendGetRequest('/okl', {'token': self.userkey, 'okl':o['name'].lower()})
                content = self.getJson(content)
                items[o['name']] = content['data']
                #return okls['data'], eivs['data'], koms['data'], oeks['data']
            return items
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            self.iface.messageBar().pushMessage('KSP',
                self.tr(u'Beim Abfragen der Daten ist ein Fehler aufgetreten.'),
                self.iface.messageBar().CRITICAL,
                duration = 3
            )
    
    def checkKennung(self, kennung, okl):
        content, response_headers = self.sendGetRequest('/okl/checkkennung', {'token': self.userkey, 'kennung':kennung, 'okl':okl})
        item = self.getJson(content)
        
        if(item['message'] == 'ok'):
            return True
        else:
            return False
        
    def getKonfiguration(self):
        try:
            content, response_headers = self.sendGetRequest('/konfiguration', {})
            
            if content is None or content == "":
                return None
            data = self.getJson(content)
            if data:
                return data.get('data')
            return None
        except:
            self.iface.messageBar().pushMessage('KSP',
                self.tr(u'Das Abfragen der Konfiguration vom Server ist fehlgeschlagen. Überprüfen Sie bitte, ob der Server erriechbar ist und starten Sie QGIS gegebenfalls neu.'),
                self.iface.messageBar().CRITICAL,
                duration = 7
            )
    
    def diviGetLayerFeatures(self, layerid):
        QgsMessageLog.logMessage('Fecthing layer %s features' % layerid, 'KSP')
        layer = self.sendGetRequest('/features/%s'%layerid, {'geometry':'base64'})
        return self.getJson(layer)
    
    def diviGetLayer(self, layerid):
        QgsMessageLog.logMessage('Fecthing layer %s' % layerid, 'KSP')
        layer = self.sendGetRequest('/layers/%s'%layerid)
        return self.getJson(layer)
    
    def sendGeoJSON(self, data, filename, projectid, data_format):
        multi_part = QHttpMultiPart(QHttpMultiPart.FormDataType)
        format_part = QHttpPart()
        format_part.setHeader(QNetworkRequest.ContentDispositionHeader, 'form-data; name="format"')
        format_part.setBody(data_format)
        file_part = QHttpPart()
        file_part.setHeader(QNetworkRequest.ContentDispositionHeader, 'form-data; name="file[0]"; filename="%s.sqlite"' % filename)
        file_part.setHeader(QNetworkRequest.ContentTypeHeader, "application/octet-stream")
        file_part.setBody(data)
        multi_part.append(format_part)
        multi_part.append(file_part)
        content, response_headers = self.sendRequest( '/upload_gis/%s/new' % projectid, {}, 'post', multi_part)
        return json.loads(content)
    
    def loadGeom(self, id, okl):
        content, response_headers = self.sendGetRequest('/okl/objekt', {'id': id, 'okl':okl, 'token': self.userkey})
        data = self.getJson(content)
        return data['data']
    
    def sendGeomToKomOn(self, id, okl, wkt, geoj):
        content, response_headers = self.sendPutRequest('/okl/objekt', {'id': id, 'wkt': wkt, 'okl': okl, 'token': self.userkey})
        data = self.getJson(content)
        return True
    
    def sendNewGeomToKomOn(self, okl, kennung, wkt, geoj):
        content, response_headers = self.sendPostRequest('/okl/objekt', {'id': 0, 'kennung': kennung, 'okl': okl, 'token': self.userkey, 'wkt': wkt})
        data = self.getJson(content)
        return data
    
    #Helpers
    
    def downloadProgress(self, received, total):
        if total!=0:
            self.downloadingProgress.emit( float(received)/total )
        else:
            self.downloadingProgress.emit( 1. )
    
    def uploadProgress(self, received, total):
        if total!=0:
            self.uploadingProgress.emit( float(received)/total )
        else:
            self.uploadingProgress.emit( 1. )
    
    def _sslError(self, reply, errors):
        reply.ignoreSslErrors()
    
    def formatUrl(self, endpoint, params={}):
        url = str(QSettings().value('komon/urlserver', 'NULL'))
        if url == 'NULL':
            self.iface.messageBar().pushMessage('Fehler',
                self.tr('Der URL des Servers ist leer.'),
                self.iface.messageBar().CRITICAL,
                duration = 3
            )
            return None
        else:
            url = QUrl(QSettings().value('komon/urlserver', '') + self.KomOn_HOST_REST + endpoint)
            url.setQueryItems(list(params.iteritems()))
            return url
    
    def deleteObject(self, id, okl):
        QgsMessageLog.logMessage(u'Objekt wird gelöscht %s' % id, 'KSP')
        content, response_headers = self.sendDeleteRequest('/okl', {'token': self.userkey, 'okl':okl, 'id':str(id)})
        return self.getJson(content)
    
    @staticmethod
    def getJson(data):
        if data:
            return json.loads(data)
        return []
