========================
|logo| KSP QGIS Plugin
========================

DESCRIPTION
+++++++++++

DIVI QGIS Plugin is extension for QGIS written in Python. It allows to view and edit data from `KSP - Das Serviceportal für Kompensationsflächen`.
Plugin is still in developement and is marked as experimental.

Plugin is based on DIVI-QGIS-Plugin `GIS Support sp. z o. o. <http://www.gis-support.com>` and it was extended by processware GmbH <http://www.processware.de>

INSTALATION
+++++++++++

DIVI QGIS Plugin is available in official QGIS plugins repository. It's marked as experimental, so you need to check *Show also experimental plugins* in Settings.

LICENSE
+++++++

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

.. |logo| image:: ./images/icon.png
