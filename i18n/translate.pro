CODECFORTR = UTF-8
FORMS = ../dialogs/dockwidget.ui ../dialogs/import_dialog.ui
SOURCES = ../divi_plugin.py \
    ../dialogs/dockwidget.py ../dialogs/import_dialog.py \
    ../utils/connector.py ../utils/model.py ../utils/widgets.py
          
TRANSLATIONS = DiviPlugin_pl.ts