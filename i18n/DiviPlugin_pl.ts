<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="pl_PL" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="109"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="25"/>
        <source>Layer:</source>
        <translation>Warstwa:</translation>
    </message>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="42"/>
        <source>Account:</source>
        <translation>Konto:</translation>
    </message>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="52"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="62"/>
        <source>Layer name:</source>
        <translation>Nazwa warstwy:</translation>
    </message>
    <message>
        <location filename="../dialogs/import_dialog.ui" line="102"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>DiviConnector</name>
    <message>
        <location filename="../utils/connector.py" line="97"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../utils/connector.py" line="74"/>
        <source>Server rejected request</source>
        <translation>Serwer odrzucił żądanie</translation>
    </message>
    <message>
        <location filename="../utils/connector.py" line="91"/>
        <source>Error 404: requested resource could not be found </source>
        <translation>Błąd 404: nie znaleziono zasobu</translation>
    </message>
    <message>
        <location filename="../utils/connector.py" line="97"/>
        <source>Error 423: requested resource is locked</source>
        <translation>Błąd 423: żądany zasób jest zablokowany do edycji</translation>
    </message>
</context>
<context>
    <name>DiviModel</name>
    <message>
        <location filename="../utils/model.py" line="185"/>
        <source>Data</source>
        <translation>Dane</translation>
    </message>
</context>
<context>
    <name>DiviPlugin</name>
    <message>
        <location filename="../divi_plugin.py" line="223"/>
        <source>&amp;DIVI QGIS Plugin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="194"/>
        <source>DIVI QGIS Plugin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="200"/>
        <source>Upload layer</source>
        <translation>Prześlij warstwę</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="234"/>
        <source>Loading new layer will be possible after current operation.</source>
        <translation>Załadowanie nowej warstwy będzie możliwe dopiero po zakończeniu trwającej operacji.</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="249"/>
        <source>Downloading layer &apos;%s&apos;...</source>
        <translation>Pobieranie warstwy &apos;%s&apos;...</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="422"/>
        <source>Saving layer %s</source>
        <translation>Zapisywanie warstwy &apos;%s&apos;</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="466"/>
        <source>Error occured while removing features. Not all features where deleted.</source>
        <translation>Podczas usuwania obiektów wystąpił błąd. Nie wszystkie obiekty zostały usunięte.</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="491"/>
        <source>Saving features to layer %s</source>
        <translation>Zapisywanie obiektów do warstwy &apos;%s&apos;</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="506"/>
        <source>Error occured while adding new features. Not all features where added.</source>
        <translation>Podczas dodawania nowych obiektów wystąpił. Nie wszystkie obiekty zostały dodane.</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="552"/>
        <source>No vector layers.</source>
        <translation>Brak warstw wektorowych.</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="434"/>
        <source>Warning:</source>
        <translation>Uwaga:</translation>
    </message>
    <message>
        <location filename="../divi_plugin.py" line="434"/>
        <source>Table schema was changed. You need to reload layers that are related to changed layer.</source>
        <translation>Zmieniono strukturę warstwy powiązanej z innymi wczytanymi warstwami DIVI.  Wczytaj ponownie warstwy aby zaktualizować ich strukturę.</translation>
    </message>
</context>
<context>
    <name>DiviPluginDockWidget</name>
    <message>
        <location filename="../dialogs/dockwidget.py" line="103"/>
        <source>Logged: %s</source>
        <translation>Zalogowany: &apos;%s&apos;</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="104"/>
        <source>Disconnect</source>
        <translation>Rozłącz</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="108"/>
        <source>Not logged</source>
        <translation>Niezalogowany</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="109"/>
        <source>Connect</source>
        <translation>Połącz</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="142"/>
        <source>Downloading layer &apos;%s&apos;...</source>
        <translation>Pobieranie warstwy &apos;%s&apos;...</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="157"/>
        <source>QGIS 2.14 or later is required for loading DIVI tables.</source>
        <translation>Aby załadować tabelę DIVI wymagane jest posiadanie QGIS w wersji 2.14 lub nowszej.</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="163"/>
        <source>Downloading table &apos;%s&apos;...</source>
        <translation>Pobieranie tabeli &apos;%s&apos;...</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="183"/>
        <source>One of related layers is in edit mode. End edit mode of that layer to continue.</source>
        <translation>Jedna z powiązanych warstw jest w trybie edycji. Zakończ edycję aby kontynuować.</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="203"/>
        <source>Add layer</source>
        <translation>Dodaj warstwę</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="204"/>
        <source>Add layer as...</source>
        <translation>Dodaj warstwę jako...</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="209"/>
        <source>Change layer name...</source>
        <translation>Zmień nazwę warstwy...</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="211"/>
        <source>Reload data</source>
        <translation>Pobierz ponownie</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="213"/>
        <source>Add all layers from project</source>
        <translation>Dodaj warstwy z projektu</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="224"/>
        <source>Removing layer %s</source>
        <translation>Usuń warstwę &apos;%s&apos;</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="244"/>
        <source>Change name</source>
        <translation>Zmień nazwę</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="244"/>
        <source>Enter new layer name for %s</source>
        <translation>Podaj nową nazwę dla warstwy &apos;%s&apos;</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="250"/>
        <source>Name of layer %s was changed to %s.</source>
        <translation>Nazwa warstwy &apos;%s&apos; została zmieniona na&apos;%s&apos;. </translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="257"/>
        <source>Error occured while changing name.</source>
        <translation>Wystąpił błąd podczas zmiany nazwy.</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="206"/>
        <source>Points</source>
        <translation>Punkty</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="207"/>
        <source>Linestring</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.py" line="208"/>
        <source>Polygons</source>
        <translation>Poligony</translation>
    </message>
</context>
<context>
    <name>DiviPluginDockWidgetBase</name>
    <message>
        <location filename="../dialogs/dockwidget.ui" line="17"/>
        <source>DIVI QGIS Plugin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.ui" line="46"/>
        <source>Connect</source>
        <translation>Połącz</translation>
    </message>
    <message>
        <location filename="../dialogs/dockwidget.ui" line="58"/>
        <source>Search...</source>
        <translation>Szukaj...</translation>
    </message>
</context>
<context>
    <name>DiviPluginImportDialog</name>
    <message>
        <location filename="../dialogs/import_dialog.py" line="105"/>
        <source>Uploading layer &apos;%s&apos;...</source>
        <translation>Ładowanie warstwy &apos;%s&apos;...</translation>
    </message>
</context>
<context>
    <name>LoadingItem</name>
    <message>
        <location filename="../utils/model.py" line="82"/>
        <source>Downloading data...</source>
        <translation>Pobieranie danych...</translation>
    </message>
</context>
<context>
    <name>ProgressMessageBar</name>
    <message>
        <location filename="../utils/widgets.py" line="56"/>
        <source>Loading layer {} %</source>
        <translation>Ładowanie warstwy {} %</translation>
    </message>
</context>
</TS>
