# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSP-Plugin
 QGIS-Plugin zur Datenerfassung für das Serviceportal für Kompensationsflächen
                             -------------------
        begin                : 2017-01-01
        copyright            : (C) 2017 by processware GmbH
        email                : support@processware.de
 ***************************************************************************/
/***************************************************************************
 DiviPlugin
                                 A QGIS plugin
 Integracja QGIS z platformą DIVI firmy GIS Support sp. z o. o.
                              -------------------
        begin                : 2016-02-09
        git sha              : $Format:%H$
        copyright            : (C) 2016 by GIS Support sp. z o. o.
        email                : info@gis-support.pl
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QObject, QSettings, QTranslator, qVersion, QCoreApplication, Qt,\
    QVariant, QObject, QPyNullVariant, QDateTime, SIGNAL
from PyQt4.QtGui import QAction, QIcon, QMessageBox
from PyQt4.Qt import QDialog
from qgis.core import QgsProject, QGis, QgsVectorLayer, QgsMessageLog,\
    QgsMapLayerRegistry, QgsField, QgsFeature, QgsGeometry, QgsFeatureRequest,\
    QgsApplication

# Import the code for the DockWidget
from dialogs.dockwidget import DiviPluginDockWidget
import os.path
from functools import partial
from base64 import b64decode

from .utils.connector import DiviConnector
from .utils.widgets import ProgressMessageBar
from dialogs.einstellungen.einstellungen import Ui_Einstellungen

class KomonPlugin(QObject):
    """QGIS Plugin Implementation."""
    
    TYPES_MAP = {
        'number' : QVariant.Double,
        'calendar' : QVariant.DateTime,
    }
    
    QGIS2DIVI_TYPES_MAP = {
        'number' : (QVariant.Int, QVariant.Double, QVariant.UInt, QVariant.ULongLong),
        'date' : (QVariant.Date, QVariant.DateTime)
    }

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        super(KomonPlugin, self).__init__()
        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        locale = QSettings().value('locale/userLocale')
        if locale is (None, QPyNullVariant):
            locale = "DE"
        else:
            locale = QSettings().value('locale/userLocale')[0:2]
            
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'KomonPlugin_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = '&SP-Plugin'

        self.msgBar = None
        self.ids_map = {}
        self.loading = False
        self.cache = {}

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('KomonPlugin', message)


    def add_action(
        self,
        icon_path,
        text,
        callback=None,
        action=None,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        if action is None:
            action = QAction(QIcon(icon_path), text, parent)
        else:
            action.setIcon(QIcon(icon_path))
            action.setText(text)
        if callback is not None:
            action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.iface.addWebToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToWebMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action


    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        
        #self.iface.projectRead.connect(self.loadLayers)
        
        icon_path = os.path.join(self.plugin_dir,'images/icon.png')
        self.dockwidget = DiviPluginDockWidget(self)
        
        self.actionKomOn = self.add_action(
            icon_path,
            u'Oberfläche Anzeigen',
            action = self.dockwidget.toggleViewAction(),
            parent=self.iface.mainWindow())
        
        self.actionEinstellungen = self.add_action(
            icon_path,
            'Einstellungen',
            callback = self.zeigeEinstellungen,
            parent=self.iface.mainWindow(),
            add_to_toolbar = False)
        
        self.actionUeber = self.add_action(
            icon_path,
            u'Über SP-QGIS-Plugin',
            callback = self.zeigeInfo,
            parent=self.iface.mainWindow(),
            add_to_toolbar = False)
        
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dockwidget)
        
    #--------------------------------------------------------------------------
    
    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        
        #print "** UNLOAD KSP-Plugin"
        
        try:
            self.iface.projectRead.disconnect(self.loadLayers)
            QgsMapLayerRegistry.instance().layersWillBeRemoved.disconnect( self.dockwidget.layersRemoved )
        except:
            pass
        
        #Entfernen der LogIn-Daten
        settings = QSettings()
        settings.setValue('komon/userkey', None)
        settings.setValue('komon/token', None)
        
        #Disconnect layers signal
        for layer in [ layer for layer in QgsMapLayerRegistry.instance().mapLayers().itervalues() if layer.customProperty('DiviId') is not None ]:
            self.unregisterLayer(layer)

        self.iface.removePluginWebMenu(self.menu, self.actionKomOn)
        self.iface.removePluginWebMenu(self.menu, self.actionEinstellungen)
        self.iface.removePluginWebMenu(self.menu, self.actionUeber)
        self.iface.removeWebToolBarIcon(self.actionKomOn)
        
        self.iface.removeDockWidget(self.dockwidget)

    #--------------------------------------------------------------------------
    
    def loadLayers(self):
        """ Load DIVI layers after openig project """
        #Cache is used for storing data while reading project to prevent multiple connections for one layer with many geometry types
        self.cache = {}
        for layer in QgsMapLayerRegistry.instance().mapLayers().itervalues():
            self.loadLayer(layer)
        #Clear cache
        self.cache = {}
    
    def loadLayer(self, mapLayer, add_empty=False):
        divi_id = mapLayer.customProperty('DiviId')
        layer_meta = None
        settings = QSettings()
        
        if divi_id is not None:
            self.msgBar = ProgressMessageBar(self.iface, self.tr(u"Downloading layer '%s'...")%mapLayer.name(), 5, 5)
            connector = DiviConnector()
            connector.downloadingProgress.connect(self.updateDownloadProgress)
            self.msgBar.progress.setValue(5)
            
            if mapLayer.geometryType() == QGis.NoGeometry:
                layer_meta = connector.diviGetTable(divi_id)
                self.msgBar.setBoundries(10, 35)
                data = connector.diviGetTableRecords(divi_id)
                if data:
                    self.msgBar.setBoundries(45, 5)
                    permissions = connector.getUserPermissions('tables')
                    self.msgBar.setBoundries(50, 50)
                    self.addRecords(divi_id, data['header'], data['data'], fields=layer_meta['fields'], table=mapLayer, permissions=permissions)
            else:
                self.msgBar.progress.setValue(10)
                if divi_id in self.cache:
                    #Read data from cache
                    layer_meta = self.cache[divi_id]['meta']
                    data = self.cache[divi_id]['data']
                    self.msgBar.progress.setValue(35)
                else:
                    #Download data
                    layer_meta = connector.diviGetLayer(divi_id)
                    self.msgBar.setBoundries(10, 35)
                    data = connector.diviGetLayerFeatures(divi_id)
                    self.cache[divi_id] = {'data':data, 'meta':layer_meta}
                if data:
                    if mapLayer.geometryType() == QGis.Point:
                        layer = {'points':mapLayer}
                    elif mapLayer.geometryType() == QGis.Line:
                        layer = {'lines':mapLayer}
                    elif mapLayer.geometryType() == QGis.Polygon:
                        layer = {'polygons':mapLayer}
                    else:
                        return
                    self.msgBar.setBoundries(45, 5)
                    if 'permissions' in self.cache[divi_id]:
                        #Read permissions from cache
                        permissions = self.cache[divi_id]['permissions']
                    else:
                        permissions = connector.getUserPermissions('layers')
                        self.cache[divi_id]['permissions'] = permissions
                    self.msgBar.setBoundries(50, 50)
                    self.addFeatures(divi_id, data['features'], fields=layer_meta['fields'],permissions=permissions,add_empty=add_empty,**layer)
            self.msgBar.progress.setValue(100)
            self.msgBar.close()
            self.msgBar = None
        if self.dockwidget is not None:
            self.dockwidget.getLoadedDiviLayers([mapLayer])
            
        return layer_meta
    
    def loadLayerType(self, item, geom_type):
        layer = QgsVectorLayer("%s?crs=epsg:25832" % geom_type, item.name, "memory")
        layer.setCustomProperty('DiviId', item.id)
        self.loadLayer(layer, add_empty=True)
    
    def addLayer(self, features, layer, permissions={}):
        #Layers have CRS==25832
        definition = '?crs=epsg:25832'
        #Create temp layers for point, linestring and polygon geometry types
        points = QgsVectorLayer("MultiPoint"+definition, layer.name, "memory")
        lines = QgsVectorLayer("MultiLineString"+definition, layer.name, "memory")
        polygons = QgsVectorLayer("MultiPolygon"+definition, layer.name, "memory")
        return self.addFeatures(layer.id, features, fields=layer.fields,
            points=points, lines=lines, polygons=polygons, permissions=permissions)
    
    @staticmethod
    def fix_value(value):
        if isinstance(value, QPyNullVariant):
            return None
        elif isinstance(value, QDateTime):
            if value.isNull() or not value.isValid():
                return None
            else:
                return value.toString('yyyy-MM-dd hh:mm:ss')
        return value
    
    def zeigeInfo(self):
        infoString = "<table><tr><td colspan=\"2\"><b>SP-QGIS-Plugin V. 0.9</b></td></tr><tr><td colspan=\"2\"></td></tr><tr><td>Author:</td><td>processware GmbH</td></tr><tr><td>Mail:</td><td><a href=\"mailto:info@processware.de\">info@processware.de</a></td></tr><tr><td>Website:</td><td><a href=\"http://www.processware.de\">http://www.processware.de</a></td></tr><tr><td>Icons:</td><td><a href=\"https://icons8.com\">https://icons8.com</a></td></tr></table>"
        QMessageBox.information(self.iface.mainWindow(), "Über SP-QGIS-Plugin", infoString)
    
    
    def zeigeEinstellungen(self):
        self.einstellungen = Ui_Einstellungen()
        d = QDialog()
        self.einstellungen.setupUi(d)
        self.einstellungen.dialog = d;
        QObject.connect(self.einstellungen.btnJa, SIGNAL("clicked()"), self.zeigeEinstellungenOnJa)
        QObject.connect(self.einstellungen.btnNein, SIGNAL("clicked()"), self.zeigeEinstellungenOnNein)
        
        url = str(QSettings().value('komon/urlserver', 'NULL'))
        if url == "NULL":
            url = ''
        self.einstellungen.txtURL.insert(url)
        
        self.einstellungen.dialog.exec_()
    
    def zeigeEinstellungenOnJa(self):
        if self.einstellungen.txtURL.text() != None and self.einstellungen.txtURL.text().strip() != "":
            url = self.einstellungen.txtURL.text().strip()
            
            if url.endswith("/"):
                url = url[:-1]
            
            QSettings().setValue('komon/urlserver', self.einstellungen.txtURL.text())
            connector = self.dockwidget.getConnector()
            self.dockwidget.konfiguration = connector.getKonfiguration();
            self.einstellungen.dialog.close()
        else:
            self.iface.messageBar().pushMessage('Fehler',
                self.tr('Der URL des Servers ist leer.'),
                self.iface.messageBar().CRITICAL,
                duration = 3
            )
    
    def zeigeEinstellungenOnNein(self):
        self.einstellungen.dialog.close()
